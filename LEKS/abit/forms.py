from django import forms
from .models import MathInstance

class NewAbitForm(forms.ModelForm):
	title = forms.CharField(label='Title', max_length=250)

	class Meta:
		model = MathInstance
		fields = ('title',)