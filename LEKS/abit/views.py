from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

from django.utils import timezone
from .models import MathInstance, MathPage
from .forms import NewAbitForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db import models

from django.views import generic
from django.views.generic.list import ListView
from django.views.generic.edit import ModelFormMixin

from django.utils.safestring import mark_safe

from django.shortcuts import get_object_or_404
def index(request):
	return render(request, 'abit/math.html')

class usermaths(LoginRequiredMixin, ListView, ModelFormMixin):
	model = MathInstance
	form_class = NewAbitForm
	template_name = 'abit/mathinstance_list.html'
	paginate_by = 100

	def get_queryset(self):
		return MathInstance.objects.filter(owner=self.request.user)

	def get(self, request, *args, **kwargs):
		self.object = None
		self.form = self.get_form(self.form_class)
		# Explicitly states what get to call:
		return ListView.get(self, request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		# When the form is submitted, it will enter here
		self.object = None
		self.form = self.get_form(self.form_class)

		


		if self.form.is_valid():
			data = request.POST.dict()
			i = MathInstance()
			i.owner = self.request.user
			i.title = data.get('title')
			i.created_date = timezone.localtime()
			i.save()

			page = MathPage()
			page.content = ""
			page.instance = i
			page.save()

		# Whether the form validates or not, the view will be rendered by get()
			return self.get(request, *args, **kwargs)

	def get_context_data(self, *args, **kwargs):
		# Just include the form
		context = super(usermaths, self).get_context_data(*args, **kwargs)
		context['form'] = self.form
		return context

class AbitDetailView(LoginRequiredMixin, ListView):
	template_name = 'abit/mathinstance-detail.html'

	model = MathPage
	paginate_by = 100

	def get_queryset(self):
		abitkey = self.kwargs.get("abitkey")
		mathx = MathInstance.objects.get(pk=abitkey)
		return MathPage.objects.filter(instance=mathx)

	def get_context_data(self, **kwargs):
		ctx = super(AbitDetailView, self).get_context_data(**kwargs)
		abitkey = self.kwargs.get("abitkey")
		ctx['instance_key_json'] = abitkey
		return ctx