from django import forms
from .models import Submission

class SubmissionForm(forms.Form):
	title = forms.CharField(label='Title', max_length=250)
	text = forms.CharField(
		label='Text', 
		max_length=10000,
		widget=forms.Textarea()
		)
	text.widget.attrs.update({'id' : 'submission_input'})
	class Meta:
		model = Submission
		fields = ('title', 'text',)
	#TODO: ADD SUBMISSION CHECKER