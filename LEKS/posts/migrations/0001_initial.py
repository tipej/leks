# Generated by Django 2.1.2 on 2018-12-14 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('submission_title', models.CharField(max_length=200)),
                ('submission_text', models.CharField(max_length=1000)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('votes', models.IntegerField(default=0)),
            ],
        ),
    ]
